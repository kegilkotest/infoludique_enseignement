# Simuler une attaque MITM

- document sous licence [MIT](https://directory.fsf.org/wiki/License:MIT)
- envie de participer à son amélioration ? go fork/merge-request le [repo](https://gitlab.com/kegilko/infoludique_enseignement") contenant tous les documents "enseignement"

## Introduction

Ce document décrit un environnement à mettre en place en classe pour simuler une attaque type ['**M**an **I**n **T**he **M**iddle'](https://fr.wikipedia.org/wiki/Attaque_de_l%27homme_du_milieu).

<span style="color:red">Le contenu de ce document est bien entendu à utiliser dans un cadre et but **pédagogique**.</span>

Ce document est initialement créé pour le programme de 1ère NSI. Rappel du programme officiel : 

|Contenu|Capacités attendues|Commentaire|
|---|---|---|
|- Interaction client-serveur<br>- Requêtes HTTP<br>- Réponses du serveur|- Distinguer ce qui est exécuté sur le client ou sur le serveur et dans quel ordre<br>- Distinguer ce qui est mémorisé dans le client et retransmis au serveur<br>- Reconnaître quand et pourquoi la transmission est chiffrée|Il s’agit de fairele lien avec ce qui a été vu en classe de secondeet d’expliquer comment on peut passer des paramètres à un site grâce au protocole HTTP|
|- Formulaire d’une page Web|- Analyser le fonctionnement d’un formulaire simple<br>- Distinguer les transmissions de paramètres par les requêtes POST ou GET|Discuter les deux types de requêtes selon le type des valeurs à transmettre et/ou leur confidentialité

- [source](https://cache.media.eduscol.education.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf)
- Cette simulation couvre en majeure partie les capacités attendues du programme

## Pré-requis

 Vos apprenants doivent avoir un accès à un ordinateur. Certains ordinateurs seront utilisés comme serveur, ce qui implique qu'il faut installer dessus :
  - python v3.8 et +
  - un navigateur web à jour

Il est possible de simuler une attaque **MITM** de différentes manières : 

```diagramGenerator
graph TD
    0[Vos apprenants ont accès à des ordinateurs équipés de carte wifi ?];
    0 --&gt; |Oui| 00[Configuration 'hotspot WIFI']
    0 --&gt; |Non| 01[à creuser]
    style 00 color:blue
```

```diagramGenerator
graph TD
    0[Configuration 'hotspot WIFI'] --&gt; 00[[ordinateur de l'enseignant]]
    style 0 color:blue
    00 --&gt; 000{OS}
    000 --&gt; |ubuntu/debian| 0000(fa:fa-external-link-alt Créer un hotspot wifi)
    click 0000 "https://vitux.com/make-debian-a-wireless-access-point/" _blank
    000 --&gt; |autre linux| 0001(Créer un hotspot wifi)
    000 --&gt; |windows| 0002(Créer un hotspot wifi)
    000 --&gt; |macos| 0003(Créer un hotspot wifi)
    0000 --&gt; 00000[fa:fa-external-link-alt Installer le package tshark]
    click 00000 "https://tshark.dev/setup/install/" _blank
    0001 --&gt; 00000
    0002 --&gt; 00000
    0003 --&gt; 00000
    00000 --&gt; 000000[Connecter les appareils de vos apprenants sur votre hotspot WIFI]
```

### Utilisation de tshark

tshark permets d'afficher sur un terminal les données des packets échangés sur le réseau (packet sniffing). Pour collecter seulement les paquets http, utiliser la commande avec les paramètre suivants :

```
tshark -i wlp1s0 -Y 'http' -Tfields -e _ws.col.Protocol -e http.request.method -e ip.src -e ip.dst -e text
```

- `-Y 'http'` : ne filtre que les paquets http (les https sont automatiquement assimilés comme étant des paquets 'TCP', logique)
- `-Tfields` : permet d'afficher un résultat en fonction de collone déterminées (les '-e')

Une photo <span style="font-size:0.7em">dégueulasse</span> d'un exemple d'output de tshark (j'affiche les paquets TCP et HTTP pendant des échanges client/serveur via http)

![dd](https://i.ibb.co/8rpsJH1/IMG-20201231-161659292.jpg)

## Déroulement de la séance : réflexions

Je décris ici en grandes lignes le déroulement d'une séance. Vous pouvez bien sur faire tout autrement. Une ou plusieurs séances d'ailleurs ? à vous de voir !

Les apprenants doivent assimiler un nombre conséquents de concepts : 
- l'architecture client/serveur, l'existance des ports, les différentes couches réseaux (au moins TCP et HTTP, car pour rappel les paquets HTTP**S** sont assimilés comme étant des paquets TCP), les ip, ...
- les protocoles http et https, les requêtes type GET et POST, la création de formulaire HTML, ...
- l'analyse d'un package exécuté depuis un terminal (ici tshark)
- ...

Comme certains concepts sont relativement abstraits, il me semble judicieux de ne pas trop plonger les apprenants dans des détails techniques afin qu'ils se concentrent avant tout sur la bonne compréhension générale. C'est la raison pour laquelle je préfère faire le choix de fournir le code du serveur python aux apprenants. Vous trouverez le code : 

```python
# fait en python v3.8
from http.server import BaseHTTPRequestHandler, HTTPServer
import ssl

class ServerHandler(BaseHTTPRequestHandler):
    def _set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    # Ici sont gérées les requêtes de type GET
    def do_GET(self):
        
        # ignore request to favicon
        if self.path.endswith('favicon.ico'):
            return
        
        clientIp = str(self.client_address[0])
        clientInformations = str(self.headers)
        
        print("SERVEUR : [GET]")
        print(f" &gt; j'ai reçu une requête de type GET de la part de {clientIp}")
        print(f" &gt; de cette requête GET, je reçois automatiquement les informations suivantes : \n{clientInformations}")
        print(" &gt; je construis maintenant une page HTML que j'enverrai ensuite au client...attention c'est très rapide !")
        
        formFile = open('formulaire.html')
        formContentHTML = formFile.read()
    
        print(f" &gt; FINI ! j'ai construit la page HTML de mon côté (côté serveur donc) et je l'envoie au client {clientIp}\n\n")
        self.wfile.write(formContentHTML.encode("utf-8"))

    # Ici sont gérées les requêtes de type POST
    def do_POST(self):
        
        clientIp = str(self.client_address[0])
        
        postData = self.rfile.read(int(self.headers['Content-Length'])).decode('utf-8') # &lt;--- Gets the data itself
        
        print("SERVEUR : [POST]")
        print(f" &gt; j'ai reçu une requête de type POST de la part de {clientIp}")
        print(f" &gt; cette requête POST contient des données de formulaire, ces données sont : \n{postData}\n")
        print(f" &gt; je confirme la bonne réception du message transmis par {clientIp}")
        print(f" &gt; je construis maintenant une page HTML contenant un simple message et je l'envoie à {clientIp}")
        
        simpleMessage = "Le message a bien été transmis ! le contenu est visible sur le serveur..."
        
        self.wfile.write(simpleMessage.encode('utf-8'))

def serverStart(useProtocolHTTPS):    
    
    if(useProtocolHTTPS):
        # pour un navigateur web, saisir https://adresseDuSite:443 est pareil que https://adresseDuSite
        # le port 443 est, par convention, le port utilisé par le protocole HTTPS
        port = 443 
    else:
        # pour un navigateur web, saisir https://adresseDuSite:80 est pareil que http://adresseDuSite
        # le port 80 est, par convention, le port utilisé par le protocole HTTP
        port = 80
        
    server_address = ('', port)
    httpd = HTTPServer(server_address, ServerHandler)
    
    if(useProtocolHTTPS):
        httpd.socket = ssl.wrap_socket (httpd.socket, certfile='./server.pem', server_side=True)
        
    print("serveur : je suis démarré, je suis à l'écoute d'éventuelles requêtes ! \n")
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    print("serveur : je me stope\n")
    httpd.server_close()

# valeur de la variable à modifier selon si on souhaite utiliser le protocole HTTPS ou HTTPs
useProtocolHTTPS = False
serverStart(useProtocolHTTPS)
```

Afin que ce code soit, un minimum, compris par les apprenants (et donc utilisable/modifiable/bidouillable) doivent être au minimum au point sur l'utilisation des variables et des fonctions (entré/sortie). Les plus aguéris pourront passer du temps sur la classe ServerHandler.

Simuler une attaque informatique (#hacking) sera une bonne source de motivation pour nos informaticiens en herbe. Il sera par contre important de sensibiliser sur la pratique illégale de ce type de piratage et de rapeller qu'ici nous utilisons **notre** réseau et les données de **nos** formulaires. 

Les apprenants n'auront pas la main sur l'ordinateur faisant tourner le `paquet sniffer tool`, je n'en vois pas la pertinence vis à vis des attendus du programme.
Par contre, il faudra afficher sur grand écran les transit des paquets dans le réseaux. (j'ai hate de voir mes apprenants jouer et faire du ping pong avec les paquets ;) )

Une fois l'environnement technique OK (ex : un hotspot, les apprenants connectés dessus, tshark qui tourne...). Le but du jeu pour les apprenants sera de démarrer des serveurs et de créer un formulaire html. Le serveur sera accessible via n'importe quel appareil 'client' (ordinateur, smartphone, ...). Ils constateront le transit des données du formulaire 'en clair' et devront modifier le code du serveur afin qu'il fonctionne avec le protocole https. À ce moment-là, vos apprenants pourront constater l'absence des données en clair sur le réseau en analysant le output de tshark.

Vous pourrez également par la suite modifier la commande tshark afin d'afficher tous les paquets http et tcp du réseaux et constater avec les apprenants que les paquets initialement en http sont tous devenus des paquets tcp 'chiffrés'.