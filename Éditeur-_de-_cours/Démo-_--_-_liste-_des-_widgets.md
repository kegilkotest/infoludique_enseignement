# Démo : liste des widgets

Il est possible d'inclure à travers l'éditeur des widgets : des blocs permettant au lecteur d'interagir avec les documents.

<h2 style="border:3px solid black;background-color:#cccccc;text-align:center">Widgets fonctionnels</h2>

<h3 class="p-1" style="border:1px solid black;background-color:#dddddd;text-align:center">Créer un bloc de code python exécutable</h3>

Ce widget permet de générer un bloc de code python exécutable par la personne lisant le document. L'éditeur utilise la très simpatique librairie [brython](https://brython.info/) qui permet de jouer du code python en live sur un navigateur. 
Cette librairie est particulièrement intéressante car le code est entièrement exécuté côté client ! 

Sans plus attendre, si vous écrivez ceci dans l'éditeur :

![https://i.ibb.co/wg2Nrp0/Screenshot-from-2021-01-05-01-58-12.png](https://i.ibb.co/wg2Nrp0/Screenshot-from-2021-01-05-01-58-12.png)

Cela produit le résultat suivant : 

widget python
import sys
print(sys.version_info) # affiche la version python supportée par brython
widget

Bien sur, ce portage python n'est pas sans quelques limitations. Je vous invite à parcourir la [documentation du site brython](https://brython.info/static_doc/fr/intro.html).

<h2 style="border:3px solid black;background-color:#cccccc;text-align:center">Soon...</h2>

<h3 class="p-1" style="border:1px solid black;background-color:#dddddd;text-align:center"></h3>