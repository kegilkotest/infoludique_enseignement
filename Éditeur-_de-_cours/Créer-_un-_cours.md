# Créer un cours

Pour **créer un cours**, il suffit de cliquer sur le bouton "Créer un cours !" ci-dessous afin d'accéder à l'éditeur de cours "infoludique". 

<div class="row mb-2"><a href="Creer-_un-_cours.md/edit" class="text-center m-auto btn btn-success" type="button">Créer un cours !</a></div>

Une fois votre cours rédigé, vous pouvez le partager à vos apprenants comme suit : 
1. Enregistrez votre cours dans un fichier texte
    - soit en exportant votre cours en utilisant le bouton "Exporter..." situé dans la barre d'action en bas à gauche de la page
    - soit en copiant/collant manuellement le contenu du cours depuis l'éditeur "infoludique" dans un fichier que vous avez créé sur votre ordinateur
2. Transmettez votre fichiers à vos apprenants
3. Invitez vos apprenants à se rendre sur la [page de chargement de cours](/enseignement/Editeur-_de-_cours/Charger-_un-_cours.md) et d'y charger votre fichier

## FAQ : 

### Comment fonctionne cet éditeur ? 
Cet éditeur utilise le langage [Markdown](https://en.wikipedia.org/wiki/Markdown), un langage à la syntaxe claire, légère et faisant partie des standards. D'autres librairies sont également utilisables. (bootstrap / fontawesome / mermaid / ...)

Vous pourrez voir les principales fonctionnalité de l'éditeur "infoludique" dans la page "[Démo : fonctionnalités de l'éditeur][1]".

Il est également possible d'utiliser des widget itératifs, ces derniers sont décrits dans la page "[Démo : liste des widgets][2]"

### Help ! Mon ordinateur s'est éteint à cause d'une panne électrique ! j'étais en pleine rédaction de cours !
Don't panic ! Le cours est continuellement sauvegardé dans le [localstorage](https://fr.wikipedia.org/wiki/Stockage_web_local) de votre navigateur. Il vous suffit de revenir sur la page de l'éditeur de cours "infoludique" pour retrouver votre travail.

La sauvegarde dans le localstorage a également d'autres conséquences : 
- Le serveur qui héberge cet éditeur **ne reçoit, ne stocke ni ne traite aucune données** relatives à votre cours.
- Votre cours disparaîtra si vous remettez à zéro votre navigateur. (pensez à sauvegarder votre écrit de temps en temps  sur un fichier  part)

Dû à l'utilisation du localstorage de votre navigateur, il est <span style="color:red;font-weight:bold;">fortement déconseillé</span> d'utiliser l'éditeur de cours "infoludique" sur plusieurs onglets  la fois.

[1]: /enseignement/Editeur-_de-_cours/Demo-_--_-_fonctionnalites-_de-_l--__editeur.md
[2]: /enseignement/Editeur-_de-_cours/Demo-_--_-_liste-_des-_widgets.md