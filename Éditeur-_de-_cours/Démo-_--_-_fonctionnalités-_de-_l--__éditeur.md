# Démo : fonctionnalités de l'éditeur

+++ sommaire

L'éditeur 'infoludique' utilise le langage Markdown avec certaines extensions développées sur mesure. Vous trouverez ci-dessous les principales fonctionnalités.

## Fonctionnalités courantes

### Ajouter un titre

Si dans l'éditeur vous écrivez :
```markdown
# titre1
## titre2
### titre3
#### titre4
##### titre5
###### titre6
```

Cela produit le résultat suivant :

# titre1
## titre2
### titre3
#### titre4
##### titre5
###### titre6

---

### Formater le texte

Si dans l'éditeur vous écrivez :
```markdown
**Texte en gras**
*Texte en italic*
```
Cela produit le résultat suivant : 

**Texte en gras**

*Texte en italic*

### Ajouter une liste

Si dans l'éditeur vous écrivez :
```markdown
- ceci est une liste sans numéro
    - ceci est une sous liste sans numéro
1. ceci est une liste numéroté
2. voici le deuxième élément de la liste
    42. gogogo !
```
Cela produit le résultat suivant : 

- ceci est une liste sans numéro
    - ceci est une sous liste sans numéro
1. ceci est une liste numéroté
2. voici le deuxième élément de la liste
    42. gogogo !

### Ajouter un bloc de code

Si dans l'éditeur vous écrivez :

```markdown
    ```python
    #code python
    print(23 + 19)
    ```
    
    ```php
    //code php
    print(2 * 3 * 7)
    ```
    
    ```js
    // code js
    let chuckNorris = function () {
      return 42; 
    };
    
    console.log(chuckNorris()); // ne faites pas ça chez vous
    ```
```


Cela produit le résultat suivant : 

```python
#code python
print(23 + 19)
```

```php
//code php
print(2 * 3 * 7)
```

```js
// code js
let chuckNorris = function () {
  return 42; 
};

console.log(chuckNorris()); // ne faites pas ça chez vous
```

Il est bien sur possible d'utiliser d'autres langages !

### Ajouter un tableau

Si dans l'éditeur vous écrivez :

```markdown
|id|le saviez vous ?|
|--|--|
|1|Le cœur de la Terre est plus chaud que la surface du Soleil|
|2|42 est le numéro de l'appartement de Fox Mulder dans la série X-Files **COMME DE PAR HASARD** |
```

Cela produit le résultat suivant : 

|id|le saviez vous ?|
|--|--|
|1|Le cœur de la Terre est plus chaud que la surface du Soleil|
|2|42 est le numéro de l'appartement de Fox Mulder dans la série X-Files **COMME DE PAR HASARD** |

### Ajouter un lien

Les liens générés ouvrent automatiquement vers un nouvel onglet.

Si dans l'éditeur vous écrivez :

```markdown
Voici [un chouette son !](https://www.youtube.com/watch?v=4TKcz5AqcnQ)
```

Cela produit le résultat suivant : 

Voici [un chouette son !](https://www.youtube.com/watch?v=4TKcz5AqcnQ)

### Intégrer une image ou un gif

Si dans l'éditeur vous écrivez :

```markdown
![iceland](https://publicholidays.eu/wp-content/uploads/2018/03/EU_Iceland_English_2020_Output.jpg)
```

Cela produit le résultat suivant : 

![iceland](https://publicholidays.eu/wp-content/uploads/2018/03/EU_Iceland_English_2020_Output.jpg)

Il est également possible d'inclure des gifs, si dans l'éditeur vous écrivez :

```markdown
![amazing](https://i.ibb.co/1Lv4rP6/tenor-omg.gif)
```

Cela produit le résultat suivant : 

![amazing](https://i.ibb.co/1Lv4rP6/tenor-omg.gif)

### Intégrer une vidéo

Il est également possible d'intégrer des vidéos avec l'éditeur.
Si dans l'éditeur vous écrivez :

```
<div class="text-center">
<iframe src="https://player.vimeo.com/video/138623558" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>
</div>
```

Cela produit le résultat suivant : 

<div class="text-center">
<iframe src="https://player.vimeo.com/video/138623558" allowfullscreen="" width="560" height="315" frameborder="0"></iframe>
</div>

### Générer un sommaire

Pour générer un sommaire, rien de plus simple !

Si dans l'éditeur vous écrivez :

`+++ sommaire`

Cela produit le résultat suivant : 

+++ sommaire

Le sommaire se génère à l'aide des titres de niveau 2 à 6.

## Fonctionnalités avancées

### Utiliser du code HTML/CSS, Bootstrap et de Fontawesome

Pour un theming maximal, il est possible d'utiliser des balises HTML et d'y ajouter du code css. 
De plus, Bootstrap et [fontawesome v5](https://fontawesome.com/icons?d=gallery) s'invitent dans la partie ! 

Par exemple, si dans l'éditeur vous écrivez :

```markdown
<style>
.bgr{
    background-color:#ff8f6c;
}
.bgg{
    background-color:#76f07e;
}
.bgb{
    background-color:#94dbd4;
}
</style>
<div class="d-flex flex-wrap col-8 offset-2 border border-dark mb-3">
    <div class="p-2 m-2 border bgr"><i class="fas fa-7x fa-chess"></i></div>
    <div class="p-2 m-2 border bgg"><i class="fas fa-7x fa-cat"></i></div>
    <div class="p-2 m-2 border bgb"><i class="fas fa-7x fa-laptop-code"></i></div>
    <div class="p-2 m-2 border bgr"><i class="fas fa-7x fa-gamepad"></i></div>
    <div class="p-2 m-2 border bgg"><i class="fas fa-7x fa-music"></i></div>
    <div class="p-2 m-2 border bgb"><i class="fas fa-7x fa-plane"></i></div>
</div>
```

Cela produit le résultat suivant : 

<style>
.bgr{
    background-color:#ff8f6c;
}
.bgg{
    background-color:#76f07e;
}
.bgb{
    background-color:#94dbd4;
}
</style>
<div class="d-flex flex-wrap col-8 offset-2 border border-dark mb-3">
    <div class="p-2 m-2 border bgr"><i class="fas fa-7x fa-chess"></i></div>
    <div class="p-2 m-2 border bgg"><i class="fas fa-7x fa-cat"></i></div>
    <div class="p-2 m-2 border bgb"><i class="fas fa-7x fa-laptop-code"></i></div>
    <div class="p-2 m-2 border bgr"><i class="fas fa-7x fa-gamepad"></i></div>
    <div class="p-2 m-2 border bgg"><i class="fas fa-7x fa-music"></i></div>
    <div class="p-2 m-2 border bgb"><i class="fas fa-7x fa-plane"></i></div>
</div>

### Créer des diagrammes

L'éditeur utilise une librairie javascript permettant de générer des diagrammes tout choupi. La lib en question est [mermaid](https://mermaid-js.github.io/mermaid/#/)

Quelques exemples ci-dessous. Si dans l'éditeur vous écrivez :

![diagrammes](https://i.ibb.co/zfkZZ4C/Screenshot-from-2021-01-05-01-44-34.png)

Cela produit les résultats suivants : 

+++ diagramme
graph TD
    item1[initialisation] ==> item2[création d'un item coloté en bleu]
    style item2 color:blue
    item2 --> item3[ajout d'un item avec une icone fontawesome fab:fa-font-awesome-flag]
    item3 --> item4[je suis un lien cliquable !]
    item2 --> item4
    click item4 "https://youtu.be/4TKcz5AqcnQ" _blank
+++

+++ diagramme
sequenceDiagram
    Client->>Serveur: hey ! je souhaite accéder à la page 'se connecter'
    Serveur-->>Client: Très bien ! je l'ai construite, je te l'envoie !
+++

+++ diagramme
 classDiagram
      Animal <|-- Canard
      Animal <|-- Poisson
      Animal <|-- Zèbre
      Animal : +int age
      Animal : +String genre
      Animal: +estUnMammifère()
      class Canard{
          +String couleur
          +nager()
      }
      class Poisson{
          -int taille
          -nager()
      }
      class Zèbre{
          +bool estSauvage
          +galoper()
      }
+++
